<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="es_ES">
<defaultcodec></defaultcodec>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="" line="0"/>
        <source>Add user</source>
        <translation>Añadir usuario</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit user</source>
        <translation>Editar usuario</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Remove user</source>
        <translation>Eliminar usuario</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Manage RFID users</source>
        <translation>Gestionar usuarios RFID</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>     QPushButton {
         border: 2px solid #8f8f82;
         border-radius: 10px;
         background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                           stop: 0 #f0f0f0, stop: 1 #999999);
         min-width: 100px;
     }

     QPushButton:pressed {
         background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                           stop: 0 #dadbde, stop: 1 #f6f7fa);
     }

     QPushButton:flat {
         border: none; /* no border for a flat push button */
     }

     QPushButton:default {
         border-color: navy; /* make the default button prominent */
     }</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
</context>
<context>
    <name>UserPreferences</name>
    <message>
        <location filename="" line="0"/>
        <source>User preferences</source>
        <translation>Preferencias de usuario</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>     QPushButton {
         border: 2px solid #8f8f82;
         border-radius: 10px;
         background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                           stop: 0 #f0f0f0, stop: 1 #999999);
         min-width: 100px;
     }

     QPushButton:pressed {
         background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                           stop: 0 #dadbde, stop: 1 #f6f7fa);
     }

     QPushButton:flat {
         border: none; /* no border for a flat push button */
     }

     QPushButton:default {
         border-color: navy; /* make the default button prominent */
     } QLineEdit {
     border: 2px solid gray;
     border-radius: 10px;
     padding: 0 8px;
  selection-background-color: darkgray;
 }</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Card number:</source>
        <translation>Numero tarjeta:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>QLineEdit {
     border: 2px solid gray;
     border-radius: 10px;
     padding: 0 8px;
     background: yellow;
     selection-background-color: darkgray;
 }</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Real name:</source>
        <translation>Nombre real:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Introduce</source>
        <translation>Introducir</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Repeat</source>
        <translation>Repetir</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;By default card users belong to the &lt;span style=&quot; font-weight:600;&quot;&gt;rfid&lt;/span&gt; group, choose any additional group you want :&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;(new line)p, li { white-space: pre-wrap; }(new line)&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;(new line)&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Por defecto los usuarios pertenecen al grupo &lt;span style=&quot; font-weight:600;&quot;&gt;rfid&lt;/span&gt;, seleccione los grupos adicionales que desee :&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
</TS>
