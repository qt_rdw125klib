/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef USERMANAGER_H
#define USERMANAGER_H

#include <QMainWindow>
#include "ui_usermanager.h"

class QSettings;
/**
 * Application to manage RFID-based users.
 * This class allows to:
 * - Add users to the system database.
 * - Edit an existing user from the system database.
 * - Remove a existeing user from the system database.
 * It uses the standard system tools to do this, and so it can be easily adapted
 * to use different backends (like LDAP).
 * \author Juan González Aguilera <kde_devel@opsiland.info>
 */
class UserManager : public QMainWindow, private Ui::MainWindow
{
		Q_OBJECT

	public:
		/**
		 * Checks if it has been executed as root, if so it connects all signals and reads defaults.
		 * There is a "configured" parameter in the program settings file used to check if the user 
		 * has already configured the RFID users system, if it's not set this application will show 
		 * an OptionsDialog.to configure it.
		 * @param parent of this widget
		 * @param fl for this widget
		 */
		UserManager ( QWidget* parent = 0, Qt::WFlags fl = 0 );
		/**
		 * Does nothing.
		 */
		~UserManager();
	protected:
		/// Used to read/write the program settings. These settings are stored in a per-user way.
		QSettings *settings;
		/**
		 * Updates the user list when the user does any action that may change the user database.
		 */
		void updateUserList();
	protected slots:
		/**
		 * Shows a dialog so the user can introduce the data for a new account.
		 * \see UserPreferences
		 */
		void addUserSlot();
		/**
		 * Shows a dialog filled with the information of the selected user which lets
		 * the user to modify that user's data.
		 * \see UserPreferences
		 */
		void editUserSlot();
		/**
		 * Called when the user wants to remove an existing user from the system.
		 * It asks the user to confirm the user removal, and once confirmed asks about 
		 * removing the user's home folder too.
		 */
		void rmUserSlot();
		/**
		 * Shows a dialog allowing the user to modify the program's preferences.
		 */
		void preferencesSlot();
		/**
		 * Called when the user selection changes in some way.
		 * It enables/disables buttons (edituser&rmuser) depending on the current selection.
		 */
		void activateButtonsSlot();
};

#endif

