FORMS += usermanager.ui \
 userpreferences.ui \
 options.ui

SOURCES += main.cpp \
usermanager.cpp \
 userpreferences.cpp \
 optionsdialog.cpp
HEADERS += usermanager.h \
userpreferences.h \
 optionsdialog.h
RESOURCES += iconos.qrc

TRANSLATIONS += usermanagement.ts

TEMPLATE = app

INCLUDEPATH += ../rdw125k

LIBS += ../rdw125k/librdw125k.a \
-lcrypt
TARGETDEPS += ../rdw125k/librdw125k.a

INSTALLS += target

target.path = /usr/bin

