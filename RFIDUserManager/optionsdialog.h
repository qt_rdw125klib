/***************************************************************************
 *   Copyright (C) 2007 by Opsidao,,,   *
 *   opsi@ka-tet   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>
#include "ui_options.h"

#include <QSettings>
/**
 * A dialog to set the prefereces of the RFIDUserManager.
 * This dialog allows to configure:
 * - The default group for RFID users
 * - The base dir for all RFID users homes
 * \author Juan González Aguilera <kde_devel@opsiland.info>
 */
class OptionsDialog : public QDialog, private Ui::OptionsDialog
{
	Q_OBJECT

	public:
		/**
		 * Loads previously saved settings from /etc/rfid.conf and connects signals.
		 * @param parent of this widget
		 * @param fl for this widget
		 */
		OptionsDialog ( QWidget* parent = 0, Qt::WFlags fl = 0 );
		/**
		 * Does nothing.
		 */
		~OptionsDialog();
	protected slots:
		/**
		 * Commits the changes done by the user, if possible.
		 * It creates the group and base home dir if needed, and notifies if it can't do either.
		 */
		virtual void accept();
		/**
		 * Shows a dialog to select the base dir.
		 */
		void selectBasedirSlot();

};

#endif

