/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "userpreferences.h"
#include <QDebug>
#include <QItemSelectionModel>
#include <QMessageBox>
#include <QSettings>
#include <QProcess>
#include <QList>
#include <QListWidgetItem>

// #include <cstdio>
#include <grp.h>
#include <pwd.h>
#include <shadow.h>
#include <unistd.h>

#define MAX_WRITE_TRIES 5


UserPreferences::UserPreferences (const QString &userName )
	: QDialog ( 0, 0 ), Ui::UserPreferences(), editing(!userName.isEmpty()), settings(new QSettings("opsiland","RFIDUserManager",this)),writeProgress(new QProgressDialog("Writing card","Cancel card write",0,MAX_WRITE_TRIES))
{
	setupUi ( this );
	writeProgress->setWindowModality(Qt::WindowModal);
	settings->beginGroup("RFID");
	QString defaultGroupName = settings->value("defaultGroup","rfid").toString();
	groupText->setText(groupText->text().replace("{@}",defaultGroupName));
	settings->endGroup();
	//Get available groups
	struct group *g;	
	setgrent();
	while ((g=getgrent())!=NULL) {
		QString gName(g->gr_name);
		if(gName != defaultGroupName)
			groupList->addItem(gName);
	}
	endgrent();
	
	QObject::connect(password1,SIGNAL(textChanged( const QString& )),this,SLOT(checkPasswords()));
	QObject::connect(password2,SIGNAL(textChanged( const QString& )),this,SLOT(checkPasswords()));
	checkPasswords();//To force hiding the password ok label
	
	control.setName("/dev/ttyS0");
	control.setCardType(Rdw125Control::V1);
	QObject::connect(&control,SIGNAL(writePublicModeDone(int)),this,SLOT(slotWritePublicModeDone(int)));
	QObject::connect(&control,SIGNAL(readPublicModeDone(int, const QString&, const QString&)),this,SLOT(slotReadPublicModeDone(int, const QString&, const QString&)));
	
	if(!editing) {
		cardNumber->setText(nextValidCard());
	} else {
		cardNumber->setText(userName);
		realName->setText(realnameFor(userName));
		QStringList groups = groupsFor(userName);
		QItemSelectionModel *model = groupList->selectionModel();
		for (int i = 0; i < groupList->model()->rowCount();i++) {
			if (groups.contains(groupList->item(i)->text())) {
				model->select(groupList->model()->index(i,0),QItemSelectionModel::Select);
			}
		}
	}
}
UserPreferences::~UserPreferences()
{
}

/*$SPECIALIZATION$*/
void UserPreferences::accept()
{
	if (realName->text().isEmpty()) {
		int res = QMessageBox::question(this,
					"Real name is empty!!",
					"You didn't provide a real name, are you sure?",
     					QMessageBox::Yes | QMessageBox::No);
		if(res == QMessageBox::No) {
			return;
		}
	}
	int res = QMessageBox::question(this,
			"Write card?",
			"Do you want to write the card for this user now?",
			QMessageBox::Yes | QMessageBox::No);
	if(res == QMessageBox::Yes) {
		settings->beginGroup("RFID");
		QString rfidGroupName(settings->value("defaultGroup","rfid").toString());
		settings->endGroup();
		QString toWrite = cardNumber->text().remove(rfidGroupName);
		control.setData(toWrite);
		cardWriteTriesCount=0;
		writeProgress->setValue(0);
		writeProgress->show();
		control.open();
		control.writePublicModeA();
	} else {
		doUserAction();
	}
}

void UserPreferences::checkPasswords()
{
	bool passOk = (password1->text() == password2->text() && password1->text().length()>=4);
	if(editing && password1->text().length() == 0 && password2->text().length() == 0)
		passOk = true;
	labelOk->setVisible(passOk);
	labelNoOk->setVisible(!passOk);
	okButton->setEnabled(passOk);
}

void UserPreferences::slotWritePublicModeDone(int correct)
{
	Q_UNUSED(correct)
	control.readPublicModeA();
}

void UserPreferences::slotReadPublicModeDone(int correct, const QString & hexData, const QString & decData)
{
	Q_UNUSED(decData)
	switch(correct)
	{
		case Rdw125Control::Ok:
			if(hexData == control.data())
			{
				writeProgress->hide();
				QMessageBox::information(this,"Success","Card written succesfully");
				control.close();
				doUserAction();
				break;
			} 
			//this break is missing intentionally
		default:
			writeProgress->setValue(cardWriteTriesCount++);
			if (cardWriteTriesCount<MAX_WRITE_TRIES && !writeProgress->wasCanceled()) {
				control.writePublicModeA();
			} else {
				cardWriteTriesCount=-1;
				writeProgress->hide();
				QMessageBox::warning(this,"Failure","Impossible to write the card");
				control.close();
			}
			break;
	}
}

void UserPreferences::doUserAction()
{
	QProcess proc;
	QStringList args;
	args << "-c" << realName->text();
	settings->beginGroup("RFID");
	args << "-d" << settings->value("baseDir","/home/").toString().append(cardNumber->text());
	args << "-g" << settings->value("defaultGroup","rfid").toString();
	settings->endGroup();
	QList<QListWidgetItem*> items = groupList->selectedItems();
	if(!items.isEmpty())
	{
		QString groups;
		foreach(QListWidgetItem *item,items)
		{
			groups.append(item->text());
			groups.append(",");
		}
		groups.truncate(groups.length()-1);
		args << "-G";
		args << groups;
	}
	args << "-m";
	if(password1->text().length()>0)
		args << "-p" << crypt(password1->text().toStdString().c_str(),"aA");
	args << cardNumber->text();
	if (editing) {
		proc.start("usermod",args);
	} else {
		proc.start("useradd",args);
	}
	proc.waitForFinished();
	if (proc.exitCode()==0) {
		QMessageBox::information(this,"Success",QString("User %1 correctly").arg(editing ? "modified":"added",QString(proc.readAllStandardError())));
		QDialog::accept();
	} else {
		QMessageBox::warning(this,"Failure",QString("Impossible to %1 the user:\n%2").arg(editing ? "modify":"add",QString(proc.readAllStandardError())));
	}
}

QString UserPreferences::nextValidCard()
{	
	struct passwd *p;
	struct group *g;
	int biggestId = -1;
	settings->beginGroup("RFID");
	QString rfidGroupName(settings->value("defaultGroup","rfid").toString());
	settings->endGroup();
	QString out = QString("%990000000000").arg(rfidGroupName); //Had to use %99 instead of %1 because it was finding %10 instead, and so removing a 0
	
	setpwent();
	while((p=getpwent())!=NULL)
	{
		QString userName(p->pw_name);
		g = getgrgid(p->pw_gid);
		QString groupName(g->gr_name);
		if(groupName == rfidGroupName) {
			if (userName.startsWith(rfidGroupName)) {
				QString cNumber = userName.remove(rfidGroupName);
				int n = cNumber.toInt();
				if (n > biggestId) {
					biggestId = n;
					cNumber = QString::number(n+1);
					out = QString("%1%2").arg(rfidGroupName).arg(cNumber,10,'0');
				}
			}
		}
	}
	endpwent();
	return out;
}

QString UserPreferences::realnameFor(const QString & userName)
{
	QString out;
	struct passwd *p = getpwnam(userName.toStdString().c_str());
	if (p) {
		out = QString(p->pw_gecos);
	}
	return out;
}

QStringList UserPreferences::groupsFor(const QString & userName)
{
	QStringList out;
	struct group *g;
	setgrent();
	while ((g=getgrent())!=NULL) {
		int i = 0;
		char *member = g->gr_mem[0];
		while (member) {
			if (QString(member) == userName) {
				out << QString(g->gr_name);
			}
			member = g->gr_mem[++i];
		}
	}
	endgrent();	
	return out;
}
