/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "optionsdialog.h"
#include "usermanager.h"
#include "userpreferences.h"
#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>

UserManager::UserManager ( QWidget* parent, Qt::WFlags fl )
	: QMainWindow ( parent, fl ), Ui::MainWindow(),settings(new QSettings("opsiland","RFIDUserManager",this))
{
	setupUi ( this );
	if(getuid()!=0) {
		QMessageBox::warning(this,"Not the superuser","Sorry, you must be the superuser to run this program");
		exit(-1);
	}
	//Buttons
	QObject::connect(addUser,SIGNAL(clicked( bool )),this,SLOT(addUserSlot()));
	QObject::connect(editUser,SIGNAL(clicked( bool )),this,SLOT(editUserSlot()));
	QObject::connect(rmUser,SIGNAL(clicked( bool )),this,SLOT(rmUserSlot()));
	QObject::connect(prefs,SIGNAL(clicked( bool )),this,SLOT(preferencesSlot()));
	QObject::connect(userListWidget,SIGNAL(itemSelectionChanged()),this,SLOT(activarBotonesSlot()));
	
	//Check if the app has been already configured
	settings->beginGroup("RFID");
	bool configured = settings->value("configured",false).toBool();
	settings->endGroup();
	if(!configured)
	{
		OptionsDialog ops;
		ops.setWindowTitle("First run setup!");
		if(ops.exec() == QDialog::Accepted)
		{
			settings->beginGroup("RFID");
			settings->setValue("configured",true);
			settings->endGroup();
		}
	}
	updateUserList();
}

UserManager::~UserManager()
{
}

/*$SPECIALIZATION$*/

void UserManager::addUserSlot()
{
	UserPreferences prefs;
	prefs.exec();
	updateUserList();
}

void UserManager::editUserSlot()
{
	QList<QListWidgetItem *> items = userListWidget->selectedItems();
	UserPreferences prefs(items[0]->text());
	prefs.exec();
	updateUserList();
}

void UserManager::rmUserSlot()
{
	QString userName = userListWidget->selectedItems()[0]->text();
	int res = QMessageBox::question(this,QString("Deleting user %1").arg(userName),QString("Are you sure you want to remove the %1 user?\n\nTHIS CAN NOT BE UNDONE!!").arg(userName),QMessageBox::Ok|QMessageBox::Cancel);
	if (res == QMessageBox::Ok) {
		QProcess proc;
		QStringList args;
		int res = QMessageBox::question(this,"Remove home directory?","Do you want to remove the user's home directory too?",QMessageBox::Yes|QMessageBox::No);
		if (res == QMessageBox::Yes) {
			args << "-r";
		}
		args << userName;
		proc.start("userdel",args);
		proc.waitForFinished();
		if (proc.exitCode()!=0) {		
			QMessageBox::warning(this,"Failure",QString("Impossible to delete the user:\n%1").arg(QString(proc.readAllStandardError())));
		}
	}
	updateUserList();
}

void UserManager::preferencesSlot()
{
	OptionsDialog ops;
	ops.exec();
	updateUserList();
}

void UserManager::activateButtonsSlot()
{
	bool enable = !userListWidget->selectedItems().isEmpty();
	rmUser->setEnabled(enable);
	editUser->setEnabled(enable);
	
}
void UserManager::updateUserList()
{
	QStringList usersList;
	struct passwd *p;
	struct group *g;	
	settings->beginGroup("RFID");
	QString rfidGroupName(settings->value("defaultGroup","rfid").toString());
	settings->endGroup();

	setpwent();
	while((p=getpwent())!=NULL)
	{
		QString userName(p->pw_name);
		g = getgrgid(p->pw_gid);
		QString groupName(g->gr_name);
		if(groupName == rfidGroupName) {
			usersList << userName;
		}
	}
	endpwent();
	userListWidget->clear();
	userListWidget->addItems(usersList);
}


