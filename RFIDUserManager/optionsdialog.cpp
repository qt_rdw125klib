/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "optionsdialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QProcess>
#include <grp.h>

OptionsDialog::OptionsDialog ( QWidget* parent, Qt::WFlags fl )
		: QDialog ( parent, fl ), Ui::OptionsDialog()
{
	setupUi ( this );
	QSettings settings("/etc/rfid.conf",QSettings::NativeFormat);
	settings.beginGroup("RFID");
	defaultGroupName->setText(settings.value("defaultGroup","rfid").toString());
	baseDir->setText(settings.value("baseDir","/home/").toString());
	settings.endGroup();
	
	QObject::connect(selectBasedirButton,SIGNAL(clicked( bool )),this,SLOT(selectBasedirSlot()));
}

OptionsDialog::~OptionsDialog()
{
}

/*$SPECIALIZATION$*/

void OptionsDialog::accept()
{
	bool groupStuffOk = false, baseDirOk = false;;
	QString groupName = defaultGroupName->text();
	QString baseDirName = baseDir->text();
	
	QSettings settings("/etc/rfid.conf",QSettings::NativeFormat);
	settings.beginGroup("RFID");
	if (getgrnam(groupName.toStdString().c_str())) {
		settings.setValue("defaultGroup",groupName);
		groupStuffOk = true;
	} else {
		int res = QMessageBox::question(this,"No such group",QString("Given group (%1) doesn't exist, Do I create it?").arg(groupName),QMessageBox::Ok|QMessageBox::Cancel);
		if(res == QMessageBox::Ok)
		{
			QProcess proc;
			proc.start("groupadd",QStringList(groupName));
			proc.waitForFinished();
			if(proc.exitCode() == 0)
			{
				settings.setValue("defaultGroup",groupName);
				QMessageBox::information(this,"Success","Group added");
				groupStuffOk = true;
			} else {
				QMessageBox::warning(this,"Failure","Imposible to add group:\n" + proc.readAllStandardError());
			}
		}
	}
	QDir dir(baseDirName);
	if (!dir.exists()) {
		int res = QMessageBox::question(this,"No such base directory",QString("Given direcory (%1) doesn't exist, Do I create it?\n (Note you won't be able to add users until this directory exists and is writable)").arg(baseDirName),QMessageBox::Ok|QMessageBox::Cancel);
		if (res == QMessageBox::Ok) {
			if (dir.mkpath(baseDirName)) {
				QMessageBox::information(this,"Succes","Directory created");
				settings.setValue("baseDir",baseDirName);
				baseDirOk = true;
			} else {
				QMessageBox::warning(this,"Failure","Imposible to create directory");
			}
		}
	} else if(groupStuffOk) {
		settings.setValue("baseDir",baseDirName);
		baseDirOk = true;
	}
	if(groupStuffOk && baseDirOk)
		QDialog::accept();
	settings.endGroup();
}

void OptionsDialog::selectBasedirSlot()
{
	QString file = QFileDialog::getExistingDirectory(this,"Select the base directory",baseDir->text());
	if(!file.isEmpty())
		baseDir->setText(file);
}


