/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef USERPREFERENCES_H
#define USERPREFERENCES_H

#include <QDialog>
#include <QSettings>
#include <QProgressDialog>

#include <qt_rdw125k.h>

#include "ui_userpreferences.h"

class UserPreferences : public QDialog, private Ui::UserPreferences
{
	Q_OBJECT

	public:
		UserPreferences (const QString &userName="");
		~UserPreferences();
	protected:
		bool editing;
		int cardWriteTriesCount;
		Rdw125Control control;
		QSettings *settings;
		QProgressDialog *writeProgress;
		

		
		void doUserAction();
		QString nextValidCard();
		QString realnameFor(const QString &userName);
		QStringList groupsFor(const QString &userName);
		
	protected slots:
		void slotWritePublicModeDone(int correct);
		void slotReadPublicModeDone(int correct, const QString &hexData, const QString &decData);
		void checkPasswords();
		virtual void accept();
		
};

#endif

