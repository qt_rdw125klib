/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef LOGINMANAGER_H
#define LOGINMANAGER_H

#include <QWidget>
#include <qt_rdw125k.h>
#include "ui_loginmanager.h"

class QResizeEvent;
class QGraphicsPixmapItem;
class PasswordItem;
class QGraphicsTextItem;
class QSettings;
class QTimeLine;
/**
 * This application can be used to check user credentials based on a RFID card.
 * It does a continuous poll on the antenna to try reading a card in range, if it finds a card on range
 * it builds the correct username and asks for a passsword. Once the password is introduced it tries to authenticate
 * with that username/password pair using PAM, and if it success, it drops privileges to the given user and starts
 * a KDE session by subtituting the current process image.
 * As this program needs to be able to modify it's own euid and egid it must be run as root.
 */
class LoginManager : public QWidget, private Ui::Form
{
		Q_OBJECT

	public:
		/**
		 * Initiates instance objects, including animation timelines and item for the main QGraphicsScene.
		 * It also connect signals from the animation timelines to do the animations and starts the periodic
		 * card read needed.
		 * @param parent of this widget.
		 * @param fl flags for this widget.
		 */
		LoginManager ( QWidget* parent = 0, Qt::WFlags fl = 0 );
		/**
		 * Does nothing.
		 */
		~LoginManager();
	public slots:
		/**
		 * Tries to load a jpg image from the given path to be used as the background of the application.
		 * FIXME: This method doesn't complain if it can't load the image.
	 	 * @param filePath Path to the given image, as supported by #QPixmap(const QString&, const QString&).
		 */
		void setBackgroundImage(const QString &filePath);

	protected:
		/**
		 * This method is called whenever a key is presed within this widget.
		 * If there is a card in range this method identifies the key pressed and updates the passwordItem contents in 
		 * consequence.
		 * @param event that encapsulates information about the pressed key.
		 */
		virtual void keyPressEvent ( QKeyEvent * event );
		/**
		 * This method is called if the size of this widget changes in any way.
		 * It's used to rearrange the items of the scenegraph to fit on the new size.
		 * @param event encapsulating the new and old sizes of this widget.
		 */
		virtual void resizeEvent ( QResizeEvent * event );
		/**
		 * Used to change the shown items from the scenegraph depending on whether there's a card on range or not.
		 * @param onRange true if there is a card on range, false otherwise.
		 * @param card if onRange is true, it has the data stored in the card, else it's an empty string.
		 */
		void setCardOnRange(bool onRange, const QString &card = "");
		/**
		 * This method is called once the user has introduced the password.
		 * It tries to use the credentials available to authenticate against the PAM system, and
		 * if it succeds, it starts the loginSuccessAnimation, which once finished will trigger the KDE session start.
		 * \see execLogin()
		 * 
		 */
		void doLogin();
		/// The background item that shows the image set using setBackgroundImage(const QString&) method.
		QGraphicsPixmapItem *backgroundItem;
		/// The text that gives the welcome to the user, asks them about their password and greet them once authenticated.
		QGraphicsTextItem *text;
		/// A text item that's animated to notify the user if the authentication process fails.
		QGraphicsTextItem *failureNotification;;
		/// The visual item that gives feedback to the users when they're writing their password up (the password box).
		PasswordItem *passwordItem;
		/// Pixmap used by #backgroundItem to draw the background.
		QPixmap pxm;
		/// Indicates if there's a card on range.
		bool cardOnRange;
		/// The data on the card that's on range
		QString activeCard;
		/// The control to read the cards
		Rdw125Control control;
		/// Keeps the settings.
		QSettings *settings;
		/// Used to animate #failureNotification so it slides in and out from the side of the screen.
		QTimeLine *loginFailureAnimation;
		/** 
		 * Used to move and scale the passwordItem down the screen on login success.
		 * The QTimeLine::finished() signal of this timeline is connected to execLogin().
		 */
		QTimeLine *loginSuccessAnimation;
		/// This is used to avoid preocessing keypress events once the user is already authenticated.
		bool userAlreadyLogged;
	protected slots:
		/**
		 * Gets the result of the last Rdw125Control::readPublicModeA() call.
		 * It calls setCardOnRange(bool,const QString &) with the adequate parameters,
		 * and in every case calls Rdw125Control::readPublicModeA() again.
		 * @param correct one from Rdw125Control::OperationResult.
		 * @param hexData data from the read card, if any, in hexadecimal.
		 * @param decData data from the read card, if any, in decimal.
		 */
		void slotReadPublicModeDone(int correct, const QString &hexData, const QString &decData);

		/**
		 * This does the animation of #failureNotification by setting it's X position.
		 * The Y position is not modified.
		 * @param xPos The new X position.
		 */
		void slotAnimateLoginFailure(int xPos);
		/**
		 * It hides the #failureNotification item if it's finishing in the frame for time=0.
		 * The animation is only finished on the frame for time=0 when it has been run in reverse mode,
		 * and this only happens when the animation was run to hide the #failureNotification item
		 */
		void slotHideFailure();

		/**
		 * Animates the #passwordItem along the y axis when the user has been authenticated.
		 * @param yPos The new Y position.
		 */
		void slotAnimateLoginSuccess(int yPos);
		void execLogin();
};

#endif

