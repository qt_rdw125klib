/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "loginmanager.h"
#include "passworditem.h"

#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include <QGraphicsScene>
#include <QHostInfo>
#include <QPixmap>
#include <QResizeEvent>
#include <QSize>
#include <QSettings>
#include <QProcess>
#include <QTimeLine>

#include <security/pam_misc.h>
#include <security/pam_appl.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <pwd.h>
#include <grp.h>

QString currentPassword;
extern "C" int Conversation(int num_msg, const pam_message **msg,
			    pam_response **resp, void *appdata_ptr)
{
	Q_UNUSED(appdata_ptr)
	const struct pam_message *m = *msg;
	*resp = (struct pam_response*)malloc(sizeof(struct pam_response));
	struct pam_response *r = *resp;
	while ( num_msg-- ) {
		switch (m->msg_style) {
			case PAM_PROMPT_ECHO_ON:
			case PAM_PROMPT_ECHO_OFF:
				r->resp = strdup(currentPassword.toStdString().c_str());
				m++; r++;
				break;
			default:
				break;
		}
	}
	return PAM_SUCCESS;
}
static struct pam_conv pam_conv = { Conversation, NULL };

LoginManager::LoginManager ( QWidget* parent, Qt::WFlags fl )
	: QWidget ( parent, fl ), 
	    Ui::Form(),
	    backgroundItem(new QGraphicsPixmapItem()),
	    text(new QGraphicsTextItem(backgroundItem)),
	    failureNotification((new QGraphicsTextItem(backgroundItem))),
	    settings(new QSettings("opsiland","RFIDUserManager",this)),
	    loginFailureAnimation(new QTimeLine(2000,this)),
	    loginSuccessAnimation(new QTimeLine(3000,this)),
	    userAlreadyLogged(false)
{
	setupUi ( this );
	
	//TODO Make the serial port configurable from the manager...
	control.setName("/dev/ttyS0");
	control.open();
	control.start();

	setBackgroundImage(":wallpapers/Code_Poets_Dream.jpg");

	g->setScene(new QGraphicsScene(g));	
	g->scene()->addItem(backgroundItem);

	passwordItem = new PasswordItem(backgroundItem,g->scene());
	setCardOnRange(false);

	text->setFont(QFont("helvetica",32,QFont::Bold,false));
	text->setDefaultTextColor(QColor::fromRgb(150,150,255,100));

	failureNotification->setFont(QFont("helvetica",60,QFont::Bold,false));
	failureNotification->setDefaultTextColor(QColor::fromRgb(255,0,0,200));
	failureNotification->setPlainText("Login failed");
	failureNotification->setVisible(false);

	QObject::connect(loginFailureAnimation,SIGNAL(frameChanged( int )),this,SLOT(slotAnimateLoginFailure(int)));
	QObject::connect(loginFailureAnimation,SIGNAL(finished()),this,SLOT(slotHideFailure()));
	
	QObject::connect(loginSuccessAnimation,SIGNAL(frameChanged( int )),this,SLOT(slotAnimateLoginSuccess(int)));
	QObject::connect(loginSuccessAnimation,SIGNAL(finished()),this,SLOT(execLogin()));

	QObject::connect(&control,SIGNAL(readPublicModeDone(int, const QString&, const QString&)),this,SLOT(slotReadPublicModeDone(int, const QString&, const QString&)));
	control.readPublicModeA();
}

LoginManager::~LoginManager()
{
}

/*$SPECIALIZATION$*/

void LoginManager::resizeEvent(QResizeEvent * event)
{
	QSize size = event->size();	
	backgroundItem->setPixmap(pxm.scaled(g->width(),g->height(),Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
	
	QRectF passItemBounds = passwordItem->boundingRect();
	passwordItem->setPos((size.width()-passItemBounds.width())/2,2*text->boundingRect().height());
	
	text->setTextWidth(size.width());
	text->setPos((size.width()-text->boundingRect().width())/2,0);
	failureNotification->setPos(-failureNotification->boundingRect().width()/*(size.width()-failureNotification->boundingRect().width())/2*/,2*text->boundingRect().height()+passwordItem->boundingRect().height());
	
	g->setSceneRect(0,0,size.width(),size.height());

	loginFailureAnimation->setFrameRange((int)-failureNotification->boundingRect().width(),(int)(size.width()-failureNotification->boundingRect().width())/2);
	loginSuccessAnimation->setFrameRange((int)(2*text->boundingRect().height()),size.height());
}

void LoginManager::setBackgroundImage(const QString & filePath)
{
	pxm = QPixmap(filePath,"JPG");
}

void LoginManager::keyPressEvent(QKeyEvent * event)
{
	if(userAlreadyLogged)
		return;
	if(cardOnRange) {		
		int key =event->key();
		switch(key)
		{
			case Qt::Key_Return:
			case Qt::Key_Enter:
				doLogin();
				break;
			case Qt::Key_Escape:
				if (event->modifiers() & Qt::ControlModifier && event->modifiers()&Qt::ShiftModifier) {
					exit(0);
				}
				passwordItem->setPasswordLength(0);
				currentPassword="";
				loginFailureAnimation->setDirection(QTimeLine::Backward);
				if (loginFailureAnimation->state()!=QTimeLine::Running && failureNotification->isVisible()) {
					loginFailureAnimation->start();
				}
				break;
			case Qt::Key_Backspace:
				passwordItem->setPasswordLength(passwordItem->passwordLength()-1);
				if (currentPassword.length()>0) {
					currentPassword.remove(currentPassword.length()-1,1);
				}
				loginFailureAnimation->setDirection(QTimeLine::Backward);
				if (loginFailureAnimation->state()!=QTimeLine::Running && failureNotification->isVisible()) {
					loginFailureAnimation->start();
				}
				break;
			default:
				QString text = event->text();
				if (!text.isEmpty()) {
					passwordItem->setPasswordLength(passwordItem->passwordLength()+1);
					currentPassword.append(text);
					loginFailureAnimation->setDirection(QTimeLine::Backward);
					if (loginFailureAnimation->state()!=QTimeLine::Running && failureNotification->isVisible()) {
						loginFailureAnimation->start();
					}
				}
		}		
		QRectF passItemBounds = passwordItem->boundingRect();
		passwordItem->setPos((size().width()-passItemBounds.width())/2,2*text->boundingRect().height());
		update();
	}
}
void LoginManager::setCardOnRange(bool onRange, const QString & card)
{
	activeCard = card;
	cardOnRange = onRange;
	if (onRange) {
		settings->beginGroup("RFID");
		text->setHtml(QString("<html><center>Welcome user %1%2<br>Write your password and hit enter</center></html>").arg(settings->value("defaultGroup","rfid").toString(),card));
		settings->endGroup();
	} else {
		text->setHtml(QString("<html><center>Welcome to %1<br>Please put your card on range</center></html>").arg(QHostInfo::localHostName()));
		failureNotification->setVisible(false);
	}
	text->setPos((size().width()-text->boundingRect().width())/2,0);
	passwordItem->setVisible(onRange);
}

void LoginManager::slotReadPublicModeDone(int correct, const QString & hexData, const QString & decData)
{
	if(userAlreadyLogged)
		return;
	Q_UNUSED(decData)
	switch(correct)
	{
		case Rdw125Control::Ok:
			setCardOnRange(true,hexData);
			break;
		default:
			setCardOnRange(false);
			passwordItem->setPasswordLength(0);
			currentPassword="";
			break;
	}
	control.readPublicModeA();
	update();
}

void LoginManager::doLogin()
{
	pam_handle_t *pamh;
	struct passwd *p;
	int ret;
	QString groupName = settings->value("defaultGroup","rfid").toString();
	QString userName = QString("%1%2").arg(groupName,activeCard);
	ret = pam_start("login",userName.toStdString().c_str() , &pam_conv, &pamh);
	if ( ret == PAM_SUCCESS )
		ret = pam_authenticate(pamh, 0);
	if ( ret == PAM_SUCCESS )
		ret = pam_acct_mgmt(pamh, 0);
	
	
	if(ret != PAM_SUCCESS) {
		loginFailureAnimation->setDirection(QTimeLine::Forward);
		if (loginFailureAnimation->state()!=QTimeLine::Running) {
			loginFailureAnimation->start();
		}
		failureNotification->setVisible(true);
		pam_end(pamh, PAM_AUTH_ERR);
		pam_close_session(pamh,0);
	} else {
		userAlreadyLogged=true;
		p = getpwnam(userName.toStdString().c_str());
		text->setHtml(QString("<HTML><CENTER>Hello %1<BR>Nice to see you</CENTER></HTML>").arg(p->pw_gecos));
		text->setPos((size().width()-text->boundingRect().width())/2,0);
		loginSuccessAnimation->start(); //The finished signal will execve the environment
		pam_end(pamh, PAM_SUCCESS);
		pam_close_session(pamh,0);
	}
	
}

void LoginManager::slotAnimateLoginFailure(int xPos)
{
	failureNotification->setPos(xPos,failureNotification->pos().y());
}

void LoginManager::slotHideFailure()
{
	if(loginFailureAnimation->currentFrame() == loginFailureAnimation->frameForTime(0))
		failureNotification->setVisible(false);
}

void LoginManager::slotAnimateLoginSuccess(int yPos)
{
	passwordItem->scale(1,1-loginSuccessAnimation->currentValue()/14);
	passwordItem->setPos((size().width()-passwordItem->boundingRect().width())/2,yPos);
}

void LoginManager::execLogin()
{
	setVisible(false);

	struct passwd * p;
	QString groupName = settings->value("defaultGroup","rfid").toString();
	QString userName = QString("%1%2").arg(groupName,activeCard);

	passwordItem->setPasswordLength(0);
	currentPassword="";
	p = getpwnam(userName.toStdString().c_str());
	setgid(getgrnam(groupName.toStdString().c_str())->gr_gid);
	initgroups(userName.toStdString().c_str(),getgrnam(groupName.toStdString().c_str())->gr_gid);
	setuid(p->pw_uid);
		//FIXME hard coded environment

	char * const env[] = {"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$PATH",
		strdup(QString("HOME=%1").arg(p->pw_dir).toStdString().c_str()),
		strdup(QString("DISPLAY=%1").arg(getenv("DISPLAY")).toStdString().c_str()),
		strdup(QString("SHELL=%1").arg("/bin/bash").toStdString().c_str()),
		strdup(QString("USER=%1").arg(getenv("USER")).toStdString().c_str()), NULL };
	char * const argv[] = {"-c","/etc/X11/xinit/xinitrc",NULL};
	execve("/bin/bash",argv,env);
}
