SOURCES += main.cpp \
loginmanager.cpp \
passworditem.cpp
FORMS += loginmanager.ui

HEADERS += loginmanager.h \
passworditem.h
RESOURCES += wallpapers.qrc
TEMPLATE = app

QT += svg \
 network

INCLUDEPATH += ../rdw125k

LIBS += ../rdw125k/librdw125k.a \
-lpam \
-lpam_misc
TARGETDEPS += ../rdw125k/librdw125k.a

INSTALLS += target

target.path = /usr/bin

