/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "passworditem.h"
#include <QPainter>

#define CHAR_WIDTH 70

PasswordItem::PasswordItem(QGraphicsItem* parent, QGraphicsScene* scene)
	: QGraphicsItem(parent, scene), m_passwordLength(0)
{
	setToolTip("Type your password");
}


PasswordItem::~PasswordItem()
{
}


QRectF PasswordItem::boundingRect() const
{
	return QRectF(0,0,(m_passwordLength+1)*CHAR_WIDTH,2*CHAR_WIDTH);
}

void PasswordItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	Q_UNUSED(option)
	Q_UNUSED(widget)
	QPainterPath path;
	path.addRoundRect(0,0,(m_passwordLength+1)*CHAR_WIDTH,2*CHAR_WIDTH,25,25);

	QLinearGradient brush(0,0,0,CHAR_WIDTH);
	brush.setSpread(QGradient::ReflectSpread);
	brush.setColorAt(0,QColor::fromRgb(0,0,0));
	brush.setColorAt(1,QColor::fromRgb(50,0,200,128));
	painter->fillPath(path,brush);

	painter->drawRoundRect(0,0,(m_passwordLength+1)*CHAR_WIDTH,2*CHAR_WIDTH,25,25);

	brush.setColorAt(0,QColor::fromRgb(150,0,200));
	brush.setColorAt(1,QColor::fromRgb(0,0,100,50));
	for(int i = 0; i < m_passwordLength; i++)
	{
		brush.setStart(i*CHAR_WIDTH+CHAR_WIDTH/2, CHAR_WIDTH/2);
		brush.setFinalStop(i*CHAR_WIDTH+CHAR_WIDTH, CHAR_WIDTH);

		path = QPainterPath();
		path.addEllipse(i*CHAR_WIDTH+CHAR_WIDTH/2, CHAR_WIDTH/2, CHAR_WIDTH-10,CHAR_WIDTH-10);

		painter->fillPath(path,brush);
	}
	
}

void PasswordItem::setPasswordLength(int length)
{
	if(length >= 0) {
		prepareGeometryChange();
		m_passwordLength = length;
		update();
	}
}



int PasswordItem::passwordLength()
{
	return m_passwordLength;
}
