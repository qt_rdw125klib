/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "screenlocker.h"
#include <QDebug>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>
#include <QAction>
#include <QTimer>
#include <QProcess>
#include <unistd.h>
#include <pwd.h>

#define TRAY_ICON_NORMAL ":/images/oxygen-system-lock-screen.svg"
#define TRAY_ICON_BAD	 ":/images/bad.svg"

ScreenLocker::ScreenLocker(QWidget *parent)
	: QWidget(parent),
	  trayIcon(new QSystemTrayIcon(this)),
	  contextMenu(new QMenu(this)),
	  enableAction(0),
	  requirePasswordAction(0),
	  closeAction(0),
	  settings(new QSettings("opsiland","RFIDScreenLocker",this)),
	  keepLockedTimer(new QTimer(this))
{
	keepLockedTimer->setInterval(50);
	QObject::connect(keepLockedTimer,SIGNAL(timeout()),this,SLOT(slotKeepLocked()));
	
	enableAction = contextMenu->addAction("Enabled");
	enableAction->setCheckable(true);
	enableAction->setChecked(settings->value("enabled",true).toBool());
	QObject::connect(enableAction,SIGNAL(toggled(bool)),this,SLOT(enableToggled(bool)));
	QObject::connect(enableAction,SIGNAL(toggled(bool)),this,SLOT(updateTrayTooltip()));
	
	requirePasswordAction = contextMenu->addAction("Password required");
	requirePasswordAction->setToolTip("Indicates if the user will have to supply a password to unlock the screen");
	requirePasswordAction->setCheckable(true);
	requirePasswordAction->setChecked(settings->value("passwordRequired",true).toBool());
	QObject::connect(requirePasswordAction,SIGNAL(toggled(bool)),this,SLOT(updateTrayTooltip()));
	
	closeAction = contextMenu->addAction("Close");
	QObject::connect(closeAction,SIGNAL(triggered(bool)),this,SLOT(close()));
	
	trayIcon->setContextMenu(contextMenu);
	trayIcon->setIcon(QIcon(TRAY_ICON_NORMAL));
	trayIcon->setVisible(true);
	QTimer::singleShot(100,this,SLOT(startupNotify()));
		
	QObject::connect(&control,SIGNAL(testNodeLinkDone(int)),this,SLOT(slotTestNodeLinkDone(int)));
	QObject::connect(&control,SIGNAL(readPublicModeDone(int, const QString&, const QString&)),this,SLOT(slotReadPublicModeDone(int, const QString&, const QString&)));
	control.start();
	control.setName("/dev/ttyS0");
	if(enableAction->isChecked()) {
		control.open();
		control.testNodeLink();
	}

	updateTrayTooltip();
}


ScreenLocker::~ScreenLocker()
{
	settings->setValue("enabled",enableAction->isChecked());
	settings->setValue("passwordRequired",requirePasswordAction->isChecked());
	settings->sync();
}

void ScreenLocker::startupNotify()
{
	QString msg;
	if(enableAction->isChecked())
		msg="Remember that the screen will be locked if you take your RFID card away";
	else
		msg="The RFID Screen Locker is disabled, use the system tray icon to enable it";
	trayIcon->showMessage("RFID Screen Locker Running",msg,QSystemTrayIcon::Information);
}

void ScreenLocker::enableToggled(bool enabled)
{
	trayIcon->showMessage(QString("RFID Screen Locker %1").arg(enabled ? "Enabled":"Disabled"),QString("Use the system tray icon to %1 it").arg(enabled ? "disable":"enable"),QSystemTrayIcon::Information,6000);
	if (enabled) {
		trayIcon->setIcon(QIcon(TRAY_ICON_NORMAL));
		control.open();
		control.testNodeLink();
	}
	//FIXME The commented code produces a segfault with stack corruption
// 	else {
// 		control.close();
// 	}
}
void ScreenLocker::slotTestNodeLinkDone(int result)
{
	switch(result)
	{
		case Rdw125Control::Ok:
			control.readPublicModeA();
			break;
		default:
			control.close();
			enableAction->setChecked(false);
			trayIcon->setIcon(QIcon(TRAY_ICON_BAD));
			trayIcon->showMessage("Unable to open card reader","It was impossible to open the RFID reader\nThe Screen Locker won't work",QSystemTrayIcon::Warning);
	}
}

void ScreenLocker::slotReadPublicModeDone(int correct, const QString & hexData, const QString & decData)
{
	Q_UNUSED(decData)
	Q_UNUSED(hexData)
	QString userName(getpwuid(getuid())->pw_name);
	if (userName.endsWith(hexData)) {
		QProcess proc;
		QStringList args;
		args << "kdesktop" << "KScreensaverIface";
		switch(correct)
		{
			case Rdw125Control::Ok:
				keepLockedTimer->stop();
				if (!requirePasswordAction->isChecked()) {
					args << "quit";
					proc.start("dcop",args);
					proc.waitForFinished();
				}
				break;
			default:
				keepLockedTimer->start();
		}		
	} else
		qDebug() << "That's not the active user: " << hexData;
	if(enableAction->isChecked())
		control.readPublicModeA();
}

void ScreenLocker::updateTrayTooltip()
{
	bool enabled = enableAction->isChecked();
	bool passReq = requirePasswordAction->isChecked();
	QString passMsg = "";
	if (enabled) {
		passMsg = passReq ? "You will have to put your card on range and provide your password to unlock the screen":"The screen will be unlocked as soon as your card is back on range";		
	}
	trayIcon->setToolTip(QString("<HTML><P>The screen locker is %1</P><P>%2</P></HTML>").arg(enabled ? "enabled":"disabled",passMsg));
	requirePasswordAction->setEnabled(enabled);
}

void ScreenLocker::slotKeepLocked()
{
	QProcess proc;
	QStringList args;
	args << "kdesktop" << "KScreensaverIface" << "lock";
	proc.start("dcop",args);
	proc.waitForFinished();
}


