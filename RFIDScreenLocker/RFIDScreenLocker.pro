SOURCES += main.cpp \
screenlocker.cpp
TEMPLATE = app

INCLUDEPATH += ../rdw125k

INSTALLS += target

target.path = /usr/bin

LIBS += ../rdw125k/librdw125k.a

TARGETDEPS += ../rdw125k/librdw125k.a

RESOURCES += icons.qrc

HEADERS += screenlocker.h

