/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SCREENLOCKER_H
#define SCREENLOCKER_H

#include <QWidget>
#include <QSettings>
#include <qt_rdw125k.h>

class QMenu;
class QAction;
class QSystemTrayIcon;
class QTimer;
/**
	@author Juan González Aguilera <kde_devel@opsiland.info>
 */
class ScreenLocker : public QWidget
{
	Q_OBJECT
	public:
		ScreenLocker(QWidget *parent = 0);

		~ScreenLocker();
	private slots:
		void updateTrayTooltip();
		void startupNotify();
		void enableToggled(bool enabled);
		
		void slotTestNodeLinkDone(int result);
		void slotReadPublicModeDone(int correct, const QString &hexData, const QString &decData);
		void slotKeepLocked();
	private:
		Rdw125Control control;
		QSystemTrayIcon *trayIcon;
		QMenu *contextMenu;
		QAction *enableAction;
		QAction *requirePasswordAction;
		QAction *closeAction;
		QSettings *settings;
		QTimer *keepLockedTimer;
};

#endif
