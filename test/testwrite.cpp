/***************************************************************************
 *   Copyright (C) 2007 by Opsidao,,,   *
 *   opsi@ka-tet   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "testwrite.h"
#include <QDebug>
TestWrite::TestWrite(QObject *parent)
	: QObject(parent),actual1(0x44444),actual2(0x44444)
{
	connect(&control,SIGNAL(writePublicModeDone(int)),SLOT(avanzar(int)));
	control.start();
	control.setName("/dev/ttyS0");
	control.open();
	control.setCardType(Rdw125Control::V1);
	QString data=QString("%1").arg(actual1,5,16,QLatin1Char('0'))+QString("%1").arg(actual2,5,16,QLatin1Char('0'));
	control.setData(data);
	control.writePublicModeA();
}


TestWrite::~TestWrite()
{
}

void TestWrite::avanzar(int correcto)
{
	QString data=QString("%1").arg(actual1,5,16,QLatin1Char('0'))+QString("%1").arg(actual2,5,16,QLatin1Char('0'));
// 	if(correcto!=Rdw125Control::Ok)
		qDebug() << "Para : " <<data<< ":" << correcto;
	if(actual2==0xFFFF)
	{
		actual1++;
	}
	actual2++;
	data=QString("%1").arg(actual1,5,16,QLatin1Char('0'))+QString("%1").arg(actual2,5,16,QLatin1Char('0'));	
	control.setData(data);
	control.writePublicModeA();
}


