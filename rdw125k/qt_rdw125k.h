/***************************************************************************
 *   Copyright (C) 2007 by Juan González Aguilera                          *
 *                         (kde_devel@opsiland.info)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef KRW125CTL_H
#define KRW125CTL_H

#include <QObject>
#include <QPair>
#include <QThread>
#include <QMutex>
#include <QQueue>
#include <QSemaphore>

#include "qextserialport.h"

/**
 * Provides the slots and signals needed to use the Rdw125k antenna.
 * \note card locking is disabled in the source code, if you want to use
 * that feature you'll have to edit the method 
 * generateFrame(FrameType frameType, CardType cardType, QByteArray data,  bool lockCard).
 * \author Juan González Aguilera <kde_devel@opsiland.info>
 */
class Rdw125Control : public QThread
{
	Q_OBJECT
	Q_ENUMS(CardType)
	Q_ENUMS(OperationResult)
	Q_PROPERTY(QString data READ data WRITE setData)
	Q_PROPERTY(bool lockCard READ lockCard WRITE setLockCard)

	public:
		/// Possible results for each of the operations provided
		enum OperationResult {
			/// Operation succeded
			Ok,
			/// Operation failed, probably there wasn't a card on range...
			Failed,
			/// The port wasn't open when the operation was requested
			NotOpen,
			/// A hardware communication problem happened, i.e. the rdw125k returned an unknown answer
			OperationError
		};
		
		/// Possible type of cards supported by the antenna
		enum CardType {V0=0x00, V1=0x01};

		/**
		 * Registers some data types so they are usable on thread-crossing signals.
		 * It also "starts" this thread.
		 * @param parent of this object.
		 */
		Rdw125Control(QObject *parent = 0);
		/**
		 * Makes this thread end, and waits until it has really ended before letting the system
		 * delete this object.
		 */
		~Rdw125Control();

		/**
		 * Closes the underlying serial port, if it's opened.
		 */
		void close();

		/**
		 * Stablish the card type this control is working with.
		 * @param cardType Either Rdw125Control::V0 or Rdw125Control::V1
		 */
		void setCardType(CardType cardType);
		
		/**
		 * Gets the card type this controls is working with.
		 * @return Card type used actually.
		 */
		CardType cardType();

		/**
		 * Sets the data that will be written to the card.
		 * \note If you pass a data object that is not 10 bytes long the program will die with an assertion failed error.
		 * @param data to be written.
		 */
		void setData(const QString &data);
		/**
		 * Return the data associated with this control. It may be the data just read or the data set to be written previously.
		 * @return The data associated with this control.
		 */
		QString data();

		/**
		 * Stablish whether the next card to be written should be locked. 
		 * The default is write cards without locking them.
		 * \note You will have to edit the source code to lock cards, it's disabled by default, look at the generateFrame(FrameType frameType, CardType cardType, QByteArray data,  bool lockCard) method source.
		 * @param lock true if the card should be locked, false otherwise.
		 */
		void setLockCard(bool lock);
		
		/**
		 * Gets the current card locking behavior.
		 * @see setLockCard(bool)
		 * @return true if cards are been locked, false otherwise.
		 */
		bool lockCard();

		/**
		 * Opens the underlying serial port.
		 * @return true if the open was success, false if it wasn't.
		 */
		bool open();
		
		/**
		 * Checks if the underlying serial port is open.
		 * @return true it the port is open, false otherwise.
		 */
		bool isOpen();
		
		/**
		 * Sets the name of the underlying serial port (like COM1, /dev/ttyS0...).
		 * @param newName The common name of the serial port to open.
		 */
		void setName(QString newName);
		
		/**
		 * The working method of this thread, inherited from QThread.
		 */
		virtual void run();
		
		/**
		 * Makes this thread finish and waits until it has really finished.
		 */
		void end();

	public slots:
		/**
		 * Does a test to determine if there is a rdw125k connected and responding.
		 * This method queues the request, returns inmediatly and when done it will emit a testNodeLinkDone(int) signal with the result.
		 */
		void testNodeLink();
		/**
		 * Gets the firmware version of the currently attached rdw125k.
		 * This method queues the request, returns inmediatly and returns the result by emiting the getFirmwareVersionDone(bool, QPair<int,int> ) signal.
		 */
		void getFirmwareVersion();
		/**
		 * Tells the antenna to read a card.
		 * This method queues the request, returns inmediatly and returns the result by emitting the readPublicModeDone(int, const QString &, const QString &).
		 */
		void readPublicModeA();
		/**
		 * Tries to write the data associated using setData(QString) to a card on range.
		 * This method queues the request, returns inmediatly and notifies about the result of the operation using the writePublicModeDone(int) signal.
		 */
		void writePublicModeA();

	protected slots:
		/**
		 * \see testNodeLink()
		 */
		void _testNodelLink();
		/**
		 * \see getFirmwareVersion()
		 */
		void _getFirmwareVersion();
		/**
		 * \see readPublicModeA()
		 */
		void _readPublicModeA();
		/**
		 * \see writePublicModeA()
		 */
		void _writePublicModeA();

	signals:
		/**
		 * Emitted when a testNodeLink() request is processed.
		 * @param result One from #OperationResult
		 */
		void testNodeLinkDone(int result);
		/**
		 * Emitted when a getFirmwareVersion() is processed.
		 * @param correct true if it succeded, false if it failed.
		 * @param version ,if correct, a pair with the major/minor versions of the antenna's firmware.
		 */
		void getFirmwareVersionDone(bool correct, QPair<int,int> version );
		/**
		 * Emitted when a readPublicModeA() is processed.
		 * @param correct true if it succeded, false if it failed.
		 * @param hexData , if correct, it contains the read data in hexadecimal
		 * @param decData , if correct, it contanes the read data in decimal
		 */
		void readPublicModeDone(int correct, const QString &hexData = QString(), const QString &decData =QString());
		/**
		 * Emitted when a writePublicModeA() request is processed.
		 * @param result One from OperationResult indicating the write status.
		 */
		void writePublicModeDone(int correct);

	protected:
		/**
		 * Reads up to maxLength bytes from the underlying serial port.
	 	 * @param maxLength Maximun number of bytes to read from the underlying serial port.
		 * @return a QByteArray with the read bytes (it may be empty)
		 */
		QByteArray portRead(int maxLength);
		/**
		 * Writes the given bytes to the underlying serial port.
		 * @param data the bytes to be written.
		 */
		void portWrite(QByteArray data);
		
		/// These are the possible frame types that the control can send to the antenna
		enum FrameType {
			/// \see testNodeLink()
			TestLink,
			/// \see getFirmwareVersion()
			GetFirmwareVersion,
			/// \see readPublicModeA()
			Read125,
			/// \see writePublicModeA()
			Write125
		};
		
		/**
		 * Generates a frame to send to the antenna with the given parameters.
		 * \note Locking is disabled, look at this method source code to change this.
		 * \note If you pass a data object that is not 10 bytes long the program will die with an assertion failed error.
		 * @param frameType The type of frame to generate.
		 * @param cardType the type of card to write (either V0 or V1). This parameter is ignored if frameType isn't Write125
		 * @param data The data that will be included within the frame. This parameter is ignored if frameType isn't Write125
		 * @param lockCard Indicates if the frame should request card locking. This parameter is ignored if frameType isn't Write125
		 * @return the requested frame
		 */
		QByteArray generateFrame(FrameType frameType, CardType cardType=V0, QByteArray data = QByteArray(),  bool lockCard = false);
		
		/// This is the queue of requests to process.
		QQueue<FrameType> requestQueue;
		/// Mutex associated with the requestQueue to avoid thread synchronism problems.
		QMutex listMutex;
		
		/// Makes this working thread do passive waiting when there are no requests pending.
		QSemaphore workingSemaphore;
		
		/// The underlying serial port.
		QextSerialPort port;
		
		/// The cardtype this control is set to actually.
		CardType m_cardType;
		/// The data to be written to a card. 
		QString m_data;
		/// Indicates if card locking is enabled
		bool m_lock;
		/// Used to finalize this thread cleanly
		bool running;
};

#endif
